# DaFLKt

The data filtering language for Kotlin is an attempt to use natural language
queries for filtering data, especially objects with certain properties.

This library is written with Kotlin Multiplatform in mind and will work in
common code.

## Introduction to filtering expressions by example

Imagine a book with a title and an author. With DaFLKt you could ask your
library

```
title:(Dragons | Snakes) author:"Me, myself and I"
```

which would match the available books against these properties. In this case,
all books by `My, myself and I` would be returned that either are titled
`Dragons` or `Snakes`.

The full syntax is quite intuitive:

* Single words or quoted strings are strings, i.e. `title` is a string as well
  as `Me, myself and I`. The former is an unquoted string, the latter a quoted
  string.
* Unquoted strings may be used as field names like `title`.
* Fields can be queried using `name:expression` where expression is allowed to
  be one of the following:
  * quoted or unquoted string
  * `(expression)` where the expression in parentheses may contain only quoted
    or unquoted strings connected using one of the defined operators. An
    example of this is the disjunction `Dragons | Snakes` as above.
* Logical or may be expressed using `or` or the `|` symbol. One could rewrite
  `Dragons | Snakes` as `Dragons or Snakes` and `Dragons|Snakes` each with the
  same meaning.
* Logical and may be expressed using `and`. You may omit `and` using simple
  white space. The example query perform the logical conjunction on both field
  matching expressions.
* Logical negation may be expressed using `not` or an adjacent `-`. As such,
  `not Snakes` is equivalent to `-Snakes`. For fields, a special equality
  holds: `-title:Test` is equal to `title:-Test`.
* Whenever you want to use a keyword or symbol in one of your matching strings
  you have to quote it.

## Introduction to the API

Currently, the API is exposing two classes: `DaflQuery` and `Property`. Their
use shall be shown using a simple example on a fictional class `Library`.

```kotlin
val query = DaflQuery("""title:(Dragons | Snakes) author:"Me, myself and I"""")
Library.filter {
  query.evaluate(listOf(
    Property("title", it.title),
    Property("author", it.author)
  ))
}
```

The usage example above will cause the query to be destructured into an
expression tree which will is optimized, cached and reused. If you are not
performing this on a collection you might want to save the optimization and
caching for performance reasons. Instead, use the following:

```kotlin
query.evaluateOnce(listOf(
  Property("title", it.title),
  Property("author", it.author)
))
```

For properties, you may specify an optional matcher. If you wanted to compare
titles in lowercase, you could do

```kotlin
Property("title", it.title) { a, b -> a.toLowercase() == b.toLowercase() }
```
