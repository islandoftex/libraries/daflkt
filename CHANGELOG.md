# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2024-12-14

### Changed

* Updated dependencies.
* Updated Kotlin to 2.1.0.
* Updated Gradle to 8.10.

## [0.2.0] - 2024-06-28

### Changed

- Updated dependencies.

### Fixed

- Naming clash with the generated artifacts in the package registry.
  (see #2)

## [0.1.0] - 2020-12-24

Initial release

[Unreleased]: https://gitlab.com/islandoftex/libraries/daflkt/compare/v0.2.1...master
[0.2.1]: https://gitlab.com/islandoftex/libraries/daflkt/-/tags/v0.2.1
[0.2.0]: https://gitlab.com/islandoftex/libraries/daflkt/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/islandoftex/libraries/daflkt/-/tags/v0.1.0
