// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertFailsWith

class FaultyExpressionTest {
    @Test
    fun `unmatched quote`() {
        assertFailsWith<DaflParseException>("quote") {
            DaflParser("""test:"1""").toExpressionTree()
        }
    }
    @Test
    fun `unmatched parentheses`() {
        assertFailsWith<DaflParseException>("balanced") {
            DaflParser("test:(1").toExpressionTree()
        }
    }
}
