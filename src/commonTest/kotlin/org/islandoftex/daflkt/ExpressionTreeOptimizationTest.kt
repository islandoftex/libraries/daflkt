// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertEquals

class ExpressionTreeOptimizationTest {
    @Test
    fun `optimize away double not`() {
        assertEquals(NOTNode(NOTNode(Leaf("a", "b"))).optimize(),
            Leaf("a", "b"))
    }
    @Test
    fun `optimize away four consecutive not nodes`() {
        assertEquals(NOTNode(NOTNode(NOTNode(NOTNode(Leaf("a", "b"))))).optimize(),
            Leaf("a", "b"))
    }
    @Test
    fun `leave single not nodes`() {
        assertEquals(NOTNode(NOTNode(NOTNode(Leaf("a", "b")))).optimize(),
            NOTNode(Leaf("a", "b")))
    }

    @Test
    fun `apply de morgan to reduce not nodes for or`() {
        assertEquals(
            ORNode(NOTNode(Leaf("a", "b")),
                NOTNode(Leaf("b", "c"))).optimize(),
            NOTNode(ANDNode(Leaf("a", "b"),
                Leaf("b", "c"))))
    }
    @Test
    fun `apply de morgan to reduce not nodes for and`() {
        assertEquals(
            ANDNode(NOTNode(Leaf("a", "b")),
                NOTNode(Leaf("b", "c"))).optimize(),
            NOTNode(ORNode(Leaf("a", "b"),
                Leaf("b", "c"))))
    }
    @Test
    fun `prune equal subtrees before de morgan for and`() {
        assertEquals(
            ANDNode(NOTNode(Leaf("a", "b")),
                NOTNode(Leaf("a", "b"))).optimize(),
            NOTNode(Leaf("a", "b")))
    }
    @Test
    fun `prune equal subtrees before de morgan for or`() {
        assertEquals(
            ORNode(NOTNode(Leaf("a", "b")),
                NOTNode(Leaf("a", "b"))).optimize(),
            NOTNode(Leaf("a", "b")))
    }

    @Test
    fun `prune equal subtrees of leaves for and`() {
        assertEquals(
            ANDNode(Leaf("a", "b"),
                Leaf("a", "b")).optimize(),
            Leaf("a", "b"))
    }
    @Test
    fun `prune equal subtrees of leaves for or`() {
        assertEquals(
            ANDNode(Leaf("a", "b"),
                Leaf("a", "b")).optimize(),
            Leaf("a", "b"))
    }
    @Test
    fun `prune equal subtrees for and`() {
        assertEquals(
            ANDNode(ORNode(Leaf("a", "b"), Leaf("b", "c")),
                ORNode(Leaf("a", "b"), Leaf("b", "c"))).optimize(),
            ORNode(Leaf("a", "b"), Leaf("b", "c")))
    }
    @Test
    fun `prune equal subtrees for or`() {
        assertEquals(
            ORNode(ORNode(Leaf("a", "b"), Leaf("b", "c")),
                ORNode(Leaf("a", "b"), Leaf("b", "c"))).optimize(),
            ORNode(Leaf("a", "b"), Leaf("b", "c")))
    }
}
