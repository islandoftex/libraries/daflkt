// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExpressionTreeTest {
    @Test
    fun `quoted value`() {
        assertEquals(DaflParser("""test:"1 2"""").toExpressionTree(),
            Leaf("test", "1 2"))
    }
    @Test
    fun `quoted value in parentheses`() {
        assertEquals(DaflParser("""test:("1 2")""").toExpressionTree(),
            Leaf("test", "1 2"))
    }

    @Test
    fun `conjunctive expression keyword`() {
        assertEquals(DaflParser("test:1234 and test2:value").toExpressionTree(),
            ANDNode(Leaf("test", "1234"), Leaf("test2", "value")))
    }
    @Test
    fun `conjunctive expression no keyword`() {
        assertEquals(DaflParser("test:1234 test2:value").toExpressionTree(),
            ANDNode(Leaf("test", "1234"), Leaf("test2", "value")))
    }

    @Test
    fun `disjunctive expression keyword`() {
        assertEquals(DaflParser("test:1234 or test2:value").toExpressionTree(),
            ORNode(Leaf("test", "1234"), Leaf("test2", "value")))
    }
    @Test
    fun `disjunctive expression symbol`() {
        assertEquals(DaflParser("test:1234|test2:value").toExpressionTree(),
            ORNode(Leaf("test", "1234"), Leaf("test2", "value")))
    }
    @Test
    fun `disjunctive expression symbol space padded`() {
        assertEquals(DaflParser("test:1234 | test2:value").toExpressionTree(),
            ORNode(Leaf("test", "1234"), Leaf("test2", "value")))
    }

    @Test
    fun `negated expression symbol`() {
        assertEquals(DaflParser("-test:1234").toExpressionTree(),
            NOTNode(Leaf("test", "1234")))
    }
    @Test
    fun `negated expression symbol before value`() {
        assertEquals(DaflParser("test:-1234").toExpressionTree(),
            NOTNode(Leaf("test", "1234")))
    }
    @Test
    fun `negated expression keyword`() {
        assertEquals(DaflParser("not test:1234").toExpressionTree(),
            NOTNode(Leaf("test", "1234")))
    }

    @Test
    fun `conjunction binds left over right`() {
        assertEquals(DaflParser("test:1 and test2:2 and test3:3").toExpressionTree(),
            ANDNode(Leaf("test", "1"),
                ANDNode(Leaf("test2", "2"), Leaf("test3", "3"))))
    }
    @Test
    fun `disjunction binds left over right`() {
        assertEquals(DaflParser("test:1 or test2:2 or test3:3").toExpressionTree(),
            ORNode(Leaf("test", "1"),
                ORNode(Leaf("test2", "2"), Leaf("test3", "3"))))
    }

    @Test
    fun `parentheses break binding`() {
        assertEquals(DaflParser("(test:1 or test2:2) or test3:3").toExpressionTree(),
            ORNode(
                ORNode(Leaf("test", "1"),
                    Leaf("test2", "2")),
                Leaf("test3", "3")))
    }
}
