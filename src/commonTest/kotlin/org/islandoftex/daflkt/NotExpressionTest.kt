// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class NotExpressionTest {
    @Test
    fun `not expression falsy`() {
        assertFalse(DaflQuery("not test:1234").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `not expression truthy`() {
        assertTrue(DaflQuery("not test:1234").evaluate(listOf(
            Property("test", "123")
        )))
    }

    @Test
    fun `not expression symbol truthy`() {
        assertTrue(DaflQuery("-test:1234").evaluate(listOf(
            Property("test", "123")
        )))
    }
    @Test
    fun `not expression symbol before value truthy`() {
        assertTrue(DaflQuery("test:-1234").evaluate(listOf(
            Property("test", "123")
        )))
    }

    @Test
    fun `not expression symbol falsy`() {
        assertFalse(DaflQuery("-test:1234").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `not expression symbol before value falsy`() {
        assertFalse(DaflQuery("test:-1234").evaluate(listOf(
            Property("test", "1234")
        )))
    }
}
