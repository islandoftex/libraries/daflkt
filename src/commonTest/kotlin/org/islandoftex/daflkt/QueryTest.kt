// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertTrue

class QueryTest {
    @Test
    fun `evaluate single statement`() {
        assertTrue(DaflQuery("test:1234").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `evaluate single statement with custom matcher`() {
        assertTrue(DaflQuery("test:123xyz4").evaluate(listOf(
            Property("test", "1234") { value, toTest ->
                toTest.replace("xyz", "") == value
            }
        )))
    }
    @Test
    fun `ignore trailing space`() {
        assertTrue(DaflQuery("test:1234 ").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `ignore leading space`() {
        assertTrue(DaflQuery(" test:1234").evaluate(listOf(
            Property("test", "1234")
        )))
    }
}
