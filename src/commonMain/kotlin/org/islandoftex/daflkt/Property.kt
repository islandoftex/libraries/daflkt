// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * A property to be matched by DaFL.
 *
 * The textual representation `fieldName:value` maps an arbitrary [fieldName]
 * to an arbitrary [value]. The [matcher] will be used to decide if the value
 * provided by the user matches the expectation of the [Property].
 *
 * @param fieldName The identifier of the property. Should be unique in its
 *   context.
 * @param value The value that is expected to be matched.
 * @param matcher A function that determines whether there is a match or not.
 */
public data class Property(
    val fieldName: String,
    val value: String,
    val matcher: (String, String) -> Boolean = { v, toTest -> v == toTest }
)
