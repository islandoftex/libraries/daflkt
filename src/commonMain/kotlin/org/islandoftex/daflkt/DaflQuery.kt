// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * The main query object of DaFL. Represents the [query] string which is the
 * user input and is able to [evaluate] the expression.
 *
 * At this point, the intermediate representation as expression tree will not
 * be exposed. Queries are evaluated left-to-right, meaning that in equivalent
 * contexts `a or b or c` will always evaluate `a` before `b or c`.
 * Short-circuit evaluation is applied.
 *
 * @param query The query as provided by the user.
 */
public data class DaflQuery(val query: String) {
    /**
     * Create an optimized expression tree for better re-usability.
     */
    private val expressionTree by lazy {
        DaflParser(query.trim()).toExpressionTree().optimize()
    }

    /**
     * Evaluate the given query using the [properties] and thereby compute its
     * truth value. Will never fail, in case of failure returns `false`. Will
     * be faster in consecutive calls because the expression tree will be
     * cached.
     */
    public fun evaluate(properties: List<Property>): Boolean {
        return try {
            expressionTree.evaluate(properties)
        } catch (_: DaflException) {
            false
        }
    }

    /**
     * This is [evaluate] without caching and optimizing the expression tree.
     * If the query is only used once it might have better performance due to
     * a more direct evaluation. [properties] will be used as they are in
     * [evaluate].
     */
    public fun evaluateOnce(properties: List<Property>): Boolean {
        return try {
            DaflParser(query.trim()).toExpressionTree().evaluate(properties)
        } catch (_: DaflException) {
            false
        }
    }
}
