// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * Represent a node of the DaFL expression trees. Each node has at most two
 * children (i.e. binary tree).
 */
internal sealed class DaflNode

/**
 * Conjunction. Combines the truth values of [left] and [right] using logical
 * and.
 */
internal data class ANDNode(val left: DaflNode, val right: DaflNode) : DaflNode()

/**
 * Disjunction. Combines the truth values of [left] and [right] using logical
 * or.
 */
internal data class ORNode(val left: DaflNode, val right: DaflNode) : DaflNode()

/**
 * Negation. Its truth value is the inverse of [left]'s truth value.
 */
internal data class NOTNode(val left: DaflNode) : DaflNode()

/**
 * A leaf in the expression tree. Its truth value is determined by applying a
 * property's matcher for a property with the same [fieldName] on the [value].
 */
internal data class Leaf(val fieldName: String, val value: String) : DaflNode()

/**
 * Turn a tree of [DaflNode]s into a S-Expression representation for easier
 * debugging. Unused in the public API.
 */
@Suppress("Unused")
internal fun DaflNode.toSExp(): String =
    when (this) {
        is Leaf ->
            "(LEAF (fieldName '$fieldName') (value '$value'))"
        is NOTNode ->
            "(NOT\n" + left.toSExp().prependIndent("  ") + "\n)"
        is ORNode ->
            "(OR\n" + left.toSExp().prependIndent("  ") + "\n" +
                    right.toSExp().prependIndent("  ") + "\n)"
        is ANDNode ->
            "(AND\n" + left.toSExp().prependIndent("  ") + "\n" +
                    right.toSExp().prependIndent("  ") + "\n)"
    }

/**
 * Optimize the expression tree by eliminating unnecessary depth. Currently
 * only optimizes duplicated NOT nodes away.
 */
internal fun DaflNode.optimize(): DaflNode =
    when (this) {
        is NOTNode -> {
            if (left is NOTNode) {
                left.left.optimize()
            } else {
                NOTNode(left.optimize())
            }
        }
        is ANDNode -> {
            if (left == right) {
                // identity a and a = a for trees of equal structure
                left.optimize()
            } else if (left is NOTNode && right is NOTNode) {
                // apply de morgan (2 not nodes → 1)
                NOTNode(ORNode(left.left, right.left)).optimize()
            } else {
                ANDNode(left.optimize(), right.optimize())
            }
        }
        is ORNode ->
            if (left == right) {
                // identity a or a = a for trees of equal structure
                left.optimize()
            } else if  (left is NOTNode && right is NOTNode) {
                // apply de morgan (2 not nodes → 1)
                NOTNode(ANDNode(left.left, right.left)).optimize()
            } else {
                ORNode(left.optimize(), right.optimize())
            }
        is Leaf -> this
    }
