// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * Generic exception when interacting with the Dafl language.
 *
 * @param message The error message to show the user.
 */
public open class DaflException(message: String) : RuntimeException(message)

/**
 * Parsing exception when processing a query string into a [DaflNode].
 *
 * @param message The error message to show the user.
 */
public class DaflParseException(message: String) : DaflException(message)
